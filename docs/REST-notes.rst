
Good Design
-----------------
- Each object needs fundamentally only 2 methods
  - a method that gets/dels all instances of an object or creates new one. eg /dogs
  - a method that gets/dels a specific instance or updates one. /dogs/spot
- PUT should not create new instances or delete them, only updates.
- nouns should be in the URLS.
- plural nouns are best. 
  - e.g. /dogs/spot
  - e.g. /dogs?color=red&state=running&loc=park

Pagination
-----------------
- use base and offset

Versioning
-----------------
- put the version number in the left-most component of base url
  - e.g. /v1/dogs

Selecting
-----------------
- Field selector (e.g. linkedin API)
  - e.g. /people/~:(id,first-name,last-name,industry)

Format Selection
-------------------
- put the desired content-type as an extension on base url
  - e.g. /v1/dogs.json?....

Modeling
--------------------
- As an event-driven state machine
  - to tell a dog to bark: /dogs/spark.json?event=bark
  - to get its state: /dogs/spark.json/~:(state)



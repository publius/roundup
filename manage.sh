#!/bin/bash
# manage.sh

if [ $# -ge 1 ]; then
  cmd=$1
  case $cmd in
    tree)
      eval tree -I *.pyo -v
      ;;
    tree-html)
      eval tree -I *.pyo -v -H `pwd` >index.html
      ;;
  esac
fi

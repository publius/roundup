#!/usr/bin/env python
# setup.py

import os
from setuptools import setup

setup(
  name='Roundup',
  version='0.0.1',
  author='Daniel Gabriele',
  author_email='d.gabri3le@gmail.com',
  description='spare change to charity',
  license='BSD',
  keywords='blah blarg',
  packages=['roundup'],
  classifiers=[]
)


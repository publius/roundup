#!/usr/bin/env python
# module: ...api.v1.access_token

from ..routing import *

class AccessToken(ApiHandler):
  
  route = api_route('token')
  
  def get(self):
    self.response.write('hello')
    return self.response

  def post(self):
    ''' - Grant types: urn|ietf|params|oauth|grant-type|jwt-bearer.
        - The "assertion" variable refers to the client's signed JWT.
    '''
    grant_type = self.request.form.get('grant_type')[0]
    assertion  = self.request.form.get('assertion')[0]
    self.response.write(str(grant_type) + '\t' + str(assertion) + '\n')
    return self.response

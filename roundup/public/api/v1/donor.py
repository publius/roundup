#!/usr/bin/env python
# module: ...api.v1.donor

from roundup.backend import cassandra
from ..decorators import HTTP
from ..routing import *

from datetime import datetime
import json

import pycassa

class Donor(ApiHandler):

  route = api_route('donors')

  @HTTP('POST', 
    signed=True, 
    validators={
      'amt':float,
      'charity':unicode,
    }
  )
  def donate(self):
    k_donor = self.message.key
    k_recipient = self.payload.recipient
    donors = cassandra.ColumnFamily('donors')
    donations = cassandra.ColumnFamily('donations')
    now = datetime.now()
    donor = doners.try_get(
      k_donor, 
      upsert={
        now : 
     
    


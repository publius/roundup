#!/usr/bin/env python
# module: roundup.public.api.routing

from roundup.handlers import Handler, url_group


def api_route(namespace='', version=r'1'):
  # TODO: memorize this with a @memoized decorator
  uri = (r'api-v{0}/(?P<ns>{1})' + \
         r'(?:/(?:(?P<oid>[a-zA-Z_][\w\-]*)?)' + \
         r'(\.(?P<e>[a-zA-Z_][\w\-]*)$)?$)?').format(
    version, 
    namespace,
  )
  return uri

class Message(object):

  def __init__(self, ns, oid, e):
    self.ns = ns    # namespace
    self.oid = oid  # target object id
    self.e = e      # event

  def __repr__(self):
    return '''msg<{0}{1}{2}>'''.format(
      self.ns, 
      '/'+self.oid if self.oid else '', 
      '.'+self.e if self.e else ''
    )


## REGION: request method delegation decorators

class ApiHandler(Handler):

  __metaclass__ = url_group

  def __init__(self, *args, **kwargs):
    Handler.__init__(self, *args, **kwargs)
    self.response.content_type = 'application/json'

  def __call__(self):
    route_args = self.request.environ['route_args']
    msg = Message(
        ns=route_args['ns'],
        oid=route_args['oid'],
        e=route_args['e'],
    )
    getattr(self, msg.e, self.redirect)(msg)
    return self.response

  def redirect(self, msg):
    self.response.write('"404"')
  


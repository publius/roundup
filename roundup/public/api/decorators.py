#!/usr/bin/env python
# module: roundup.public.api.decorators

import auth

class HTTP(object):
  
  def __init__(self, method, authenticate=False):
    self.method = method
    self.authenticate = authenticate

  def __call__(self, func):
    def wrapper(*args, **kwargs):
      if self.method == args[0].request.environ['REQUEST_METHOD']:
        if self.authenticate:
          if auth.authenticate(args[0]):
            func(*args, **kwargs)
          else:
            args[0].redirect(args[1])
        else:
          func(*args, **kwargs)
      else:
        args[0].redirect(args[1])
    return wrapper


#!/usr/bin/env python
# module: roundup.public.api.auth

import hmac
import base64
import urllib as ul
from hashlib import sha256
from datetime import datetime
from collections import namedtuple
from pprint import pprint
import json


CANNONICAL_REQUEST_FSTR = '''
{0}
/

content-type:application/json
host:{1}
ru-date:{2}

{3}
{4}'''.lstrip()


def sign(
  method, 
  payload, 
  date, 
  host, 
  headers, 
  algorithm, 
  credential, 
  secret
):
  hashed_payload = sha256(payload).hexdigest()
  hashed_cannonical_request = sha256(
    CANNONICAL_REQUEST_FSTR.format(
      method, 
      host, 
      date, 
      headers, 
      hashed_payload
    )
  ).hexdigest()
  string_to_sign = '{0}\n{1}\n{2}\n{3}'.format(
    algorithm, 
    date, 
    credential, 
    hashed_cannonical_request
  )
  derived_key_1 = hmac.new(
    secret, 
    msg=date, 
    digestmod=sha256
  ).hexdigest()
  derived_key_2 = hmac.new(
    derived_key_1, 
    msg='roundup-request', 
    digestmod=sha256
  ).hexdigest()
  return hmac.new(
    derived_key_2, 
    msg=string_to_sign, 
    digestmod=sha256
  ).hexdigest()


def authenticate(self):
  env = self.request.environ
  algorithm = env.get('HTTP_RU_ALGORITHM', 'HMAC-SHA256')
  headers = env.get('HTTP_RU_SIGNED_HEADERS')
  credential = env.get('HTTP_RU_CREDENTIAL')
  signature = env.get('HTTP_RU_SIGNATURE')
  date = env.get('HTTP_RU_DATE')
  host = env.get('HTTP_HOST')
  # now validate the request...
  if credential and algorithm and date and signature and headers:
    if credential.find('.') < 0:
      return False
    else:
      method = env.get('REQUEST_METHOD')
      if env.get('CONTENT_LENGTH'):
        payload = self.request.load_body()[0].keys()[0] # get JSON
      else:
        payload = ''
      target = sign(
        method, 
        payload,
        date,
        host,
        headers,
        algorithm,
        credential,
        'private-key'
      )
      if signature == target:
        return True
  return False


#!/usr/bin/env python
# module: roundup.profiles

from datetime import timedelta

from wheezy.http import CacheProfile
from wheezy.http.cache import etag_md5crc32

from roundup.config import config

## an enum:
class Location(object):
  none   = 'none'
  server = 'server'
  client = 'client'
  both   = 'both'
  public = 'public'

## static file handler cache profile
static_cache_profile = CacheProfile(
  Location.public,
  duration=timedelta(minutes=15),
  vary_environ=['HTTP_ACCEPT_ENCODING'],
  namespace='static',
  enabled=config.getboolean('cache-profile', 'static-enabled')
)

## view handler cache profile
public_cache_profile = CacheProfile(
  Location.both,
  duration=timedelta(minutes=15),
  vary_environ=['HTTP_ACCEPT_ENCODING'],
  vary_cookies=['_a'],
  http_vary=['Cookie'],
  etag_func=etag_md5crc32,
  enabled=config.getboolean('cache-profile', 'public-enabled')
)


#!/usr/bin/env python
# module: roundup.views.root

from wheezy.http import HTTPResponse

from roundup.handlers import ViewHandler

class RootView(ViewHandler):

  route = ''

  def get(self):
    return self.render('public/root.mako', {'name':'Daniel'})

#!/usr/bin/env python
# module: roundup.urls

from datetime import timedelta

from wheezy.routing import url
from wheezy.web.handlers import file_handler

from roundup.wrappers import PublicStaticWrapper
import roundup.public.views as views
import roundup.public.api as api


## top-level static file handler
static_file_handler = PublicStaticWrapper(
  file_handler(
    root='static/', 
    age=timedelta(hours=1)
  )
)

## top-level urls for Roundup app
urls = []
urls.append(url('static/{path:any}', static_file_handler, name='static'))
urls.extend(views.urls)
urls.extend(api.v1.urls)

#!/usr/bin/env python
# module: roundup.rendering

from wheezy.html.ext.mako import whitespace_preprocessor
from wheezy.web.templates import MakoTemplate
from wheezy.caching import MemoryCache

from roundup.config import config

mako_renderer = MakoTemplate( 
  directories = [config.get('mako', 'lookup-directory')],
  module_directory = config.get('mako', 'module-directory'),
  filesystem_checks = False,
  cache = MemoryCache(),
  preprocessor = [
    whitespace_preprocessor
  ]
)

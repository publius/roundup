#!/usr/bin/env python
# module: roundup.backend.mongodb

import bson
import pymongo

# TODO: default to settings in config INI file


class document(dict):
  
  def __init__(self, *args, **kwargs):
    dict.__init__(self, *args, **kwargs)
    self.commits = {}
  
  @property
  def _id(self):
    return self.setdefault('_id', bson.ObjectId())

  @property
  def spec(self):
    return dict(_id=self._id)

  def push(self):
    global DONATIONS
    DONATIONS.update(
      self.spec, 
      {'$set':self.commits}, 
      upsert=True
    )
    self.update(self.commits)
    self.commits.clear()



HOST      = 'localhost'
PORT      = 27017
CLIENT    = pymongo.MongoClient(HOST, PORT, document_class=document)
DB        = CLIENT.roundup
DONATIONS = DB.donations

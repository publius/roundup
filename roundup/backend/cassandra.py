#!/usr/bin/env python
# module: roundup.backend.cassandra

import pycassa

keyspace = 'roundup'

servers = dict(
  management='localhost:9160',
  data=[
    'localhost:9160'
  ]
)

credentials = dict(
  username='root',
  password='tempus'
)

pool = pycassa.ConnectionPool(
  keyspace, 
  server_list=servers['data'],
  credentials=credentials,
  timeout=0.5,
  pool_size=5,
  prefill=True,
)

manager = pycassa.SystemManager(
  server=servers['management'],
  credentials=credentials,
  framed_transport=True,
  socket_factory=pycassa.default_socket_factory,
  transport_factory=pycassa.default_transport_factory,
)


def create_column_family(
  name, 
  column_validation_classes=None,
  comparator_type=pycassa.BYTES_TYPE,
  default_validation_class=pycassa.BYTES_TYPE,
  key_validation_class=pycassa.BYTES_TYPE
):
  if isinstance(comparator_type, tuple):
    comparator_type = pycassa.types.CompositeType(*comparator_type)
  if isinstance(key_validation_class, tuple):
    key_validation_class = pycassa.types.CompositeType(*key_validation_class)
  manager.create_column_family(
    keyspace,
    name,
    column_validation_classes,
    comparator_type=pycassa.BYTES_TYPE,
    default_validation_class=pycassa.BYTES_TYPE,
    key_validation_class=pycassa.BYTES_TYPE
  )

def column_family(name):
  return pycassa.ColumnFamily(pool, name)



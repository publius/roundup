#!/usr/bin/env python
# module: roundup.handlers

from wheezy.http import HTTPResponse
from wheezy.web.handlers import BaseHandler
from wheezy.routing import url

from roundup.config import config
from roundup.wrappers import PublicViewWrapper
from roundup.rendering import mako_renderer


class url_group(type):
  
  def __new__(type_, class_name, bases, attrs):
    route = attrs.get('route')
    cls = type.__new__(type_, class_name, bases, attrs)
    if route is not None and issubclass(cls, Handler):
      name = getattr(cls, 'name', cls.__name__)
      cls.url = url(route, PublicViewWrapper(cls), name=name)
      cls.urls.append(cls.url)
    elif bases[0].__name__ == 'Handler':
      cls.urls = []
    return cls


class Handler(BaseHandler):
  
  def __init__(self, *args, **kwargs):
    BaseHandler.__init__(self, *args, **kwargs)
    self.response = HTTPResponse()

  def __call__(self, *args, **kwargs):
    method = self.request.method
    if method == 'GET':
      self.get(*args, **kwargs)
    elif method == 'POST':
      self.post(*args, **kwargs)
    elif method == 'PUT':
      self.put(*args, **kwargs)
    elif method == 'DELETE':
      self.delete(*args, **kwargs)
    return self.response

  def get(self, *args, **kwargs):
    raise NotImplementedError()
  
  def put(self, *args, **kwargs):
    raise NotImplementedError()
  
  def post(self, *args, **kwargs):
    raise NotImplementedError()
  
  def delete(self, *args, **kwargs):
    raise NotImplementedError()
 

class ViewHandler(Handler):

  __metaclass__ = url_group

  def render(self, tmpl, env={}):
    self.response.write(mako_renderer(tmpl, env))
    return self.response

  

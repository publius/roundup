#!/usr/bin/env python
# module: roundup.handlers.wrapppers

from wheezy.http import CacheProfile
from wheezy.http import response_cache
from wheezy.http.transforms import gzip_transform
from wheezy.http.transforms import response_transforms
from wheezy.web.handlers import file_handler
from wheezy.routing import url

from roundup.public.profiles import (
  static_cache_profile,
  public_cache_profile
)

# caches gzip response body
CacheGzipWrapper = lambda p, level=5: lambda h: response_cache(p)(
  response_transforms(
    gzip_transform(compress_level=level)
  )(h)
)

# applies cache profile with given gzip compression level
PublicStaticWrapper = CacheGzipWrapper(static_cache_profile, level=5)

# applies to public template-rendered HTML
PublicViewWrapper = CacheGzipWrapper(public_cache_profile, level=5)

#!/usr/bin/env python
# file: roundup/__init__.py

from wheezy.http import WSGIApplication
from wheezy.web.middleware import bootstrap_defaults
from wheezy.web.middleware import path_routing_middleware_factory
from wheezy.caching import MemoryCache

from roundup.config import config
from roundup.rendering import mako_renderer
from roundup.urls import urls

############################################################
## option configuration
############################################################

options = {}

mode = config.get('runtime', 'mode')
if mode == 'mock':
  cache = MemoryCache()
   
options['http_cache'] = cache 
options['render_template'] = mako_renderer

############################################################
## WSGI point of entry:
############################################################

main = WSGIApplication(
  options = options,
  middleware = [
    bootstrap_defaults(url_mapping=urls),
    path_routing_middleware_factory
  ]
)
